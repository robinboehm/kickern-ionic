angular.module('robinboehm.kickerNear')
  .controller('NearCtrl', function ($scope, KickerData) {

    $scope.options = {
      radius : 5
      };

    $scope.search = function(){
      KickerData.near($scope.options)
        .then(function (locations) {
          $scope.locations = locations;
        });
    };

    // init
    $scope.search();


  });