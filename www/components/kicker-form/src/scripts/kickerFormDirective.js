angular.module('robinboehm.kickerForm')
  .directive('kickerForm', function () {

    return {
      restrict: 'E',
      templateUrl: 'components/kicker-form/src/templates/kickerForm.html'
    }
  });