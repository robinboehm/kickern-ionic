angular.module('robinboehm.kickerForm')
  .controller('ShowLocationCtrl', function ($scope, $stateParams, KickerData) {

    KickerData.getById($stateParams.locationId)
      .then(function (location) {
        $scope.location = location;
      });

    KickerData.getCheckIns($stateParams.locationId)
      .then(function (checkIns) {
        $scope.checkIns = checkIns;
      });

    $scope.checkIn = function(){
      KickerData.checkIn($stateParams.locationId)
        .then(function (checkIn) {
          $scope.checkIns.push(checkIn);
        });
    }


  });