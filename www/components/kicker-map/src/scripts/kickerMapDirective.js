angular.module('robinboehm.kickerMap')
  .directive('kickerMap', function ($http, $interpolate, $compile, $ionicLoading, KickerData) {
    return {
      restrict: 'E',
      scope: {
        onCreate: '&'
      },
      link: function ($scope, $element, $attr) {
        var lastInfo = null;

        function initialize(locations) {
          console.log(locations);
          var mapOptions = {
            center: new google.maps.LatLng(51.456680, 7.009991),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl: false
          };
          var heatMapData = [];
          var map = new google.maps.Map($element[0], mapOptions);
          var GeoMarker = new GeolocationMarker(map);


          $scope.centerOnMe = function () {

            $ionicLoading.show({
              template: 'Getting current location...',
              delay: 500,
              duration: 2500
            });

            navigator.geolocation.getCurrentPosition(function (pos) {
              map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
              map.setZoom(14);
              $ionicLoading.hide();
            }, function (error) {
              alert('Unable to get location: ' + error.message);
            });
          };

          map.controls[google.maps.ControlPosition.BOTTOM_LEFT]
            .push($compile('<button ng-click="centerOnMe()"><i class="icon ion-navigate"></i> Go to Current Location</button>')($scope)[0]);


          $http.get('components/kicker-map/src/templates/LocationInfoWindow.html', {cache: true})
            .then(function (response) {

              var template = response.data;

              for (var i = 0; i < locations.length; i++) {

                (function () {
                  var childScope = $scope.$new();
                  childScope.location = locations[i];
                  var infoWindow = new google.maps.InfoWindow({
                    content: $compile(template)(childScope)[0]
                  });

                  var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
                    map: map,
                    title: locations[i].name,
                    animation: google.maps.Animation.DROP,
                    icon: 'img/kicker-marker.png'
                  });

                  google.maps.event.addListener(marker, 'click', function () {
                    if (lastInfo) {
                      lastInfo.close();
                    }
                    infoWindow.open(map, marker);
                    lastInfo = infoWindow;
                  });
                })();

              }

            });


          $scope.onCreate({map: map});


        }

        KickerData
          .getAll()
          .then(initialize);
      }
    }
  });
