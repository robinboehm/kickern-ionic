// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services',
  'ngMaterial', 'ngMessages', 'uiGmapgoogle-maps',
  'robinboehm.kickerMap', 'robinboehm.kickerForm', 'robinboehm.kickerNear'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
      //    key: 'your api key',
      v: '3.17',
      libraries: 'visualization'
    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

      // setup an abstract state for the tabs directive
      .state('tab', {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html"
      })

      // Each tab has its own nav history stack:

      .state('tab.map', {
        url: '/map',
        views: {
          'tab-map': {
            templateUrl: 'templates/map/tab-map.html',
            controller: 'DashCtrl'
          }
        }
      })


      .state('tab.map-location', {
        url: '/map/location/:locationId',
        views: {
          'tab-map': {
            templateUrl: 'templates/map/map-location-show.html',
            controller: 'ShowLocationCtrl'
          }
        }
      })

      .state('tab.map-location-edit', {
        url: '/map/location/:locationId/edit',
        views: {
          'tab-map': {
            templateUrl: 'templates/map/map-location-edit.html',
            controller: 'KickerFormEditCtrl'
          }
        }
      })

      .state('tab.chats', {
        url: '/chats',
        views: {
          'tab-chats': {
            templateUrl: 'templates/tab-chats.html',
            controller: 'ChatsCtrl'
          }
        }
      })


      // NEAR
      .state('tab.near', {
        url: '/near',
        views: {
          'tab-near': {
            templateUrl: 'templates/near/tab-near.html',
            controller: 'NearCtrl'
          }
        }
      })

      .state('tab.near-location', {
        url: '/near/location/:locationId',
        views: {
          'tab-near': {
            templateUrl: 'templates/near/near-location-show.html',
            controller: 'ShowLocationCtrl'
          }
        }
      })

      .state('tab.near-location-edit', {
        url: '/near/location/:locationId/edit',
        views: {
          'tab-near': {
            templateUrl: 'templates/near/near-location-edit.html',
            controller: 'KickerFormEditCtrl'
          }
        }
      })

      .state('tab.chat-detail', {
        url: '/chats/:chatId',
        views: {
          'tab-chats': {
            templateUrl: 'templates/chat-detail.html',
            controller: 'ChatDetailCtrl'
          }
        }
      })

      .state('tab.friends', {
        url: '/friends',
        views: {
          'tab-friends': {
            templateUrl: 'templates/tab-friends.html',
            controller: 'FriendsCtrl'
          }
        }
      })
      .state('tab.friend-detail', {
        url: '/friend/:friendId',
        views: {
          'tab-friends': {
            templateUrl: 'templates/friend-detail.html',
            controller: 'FriendDetailCtrl'
          }
        }
      })

      .state('tab.account', {
        url: '/account',
        views: {
          'tab-account': {
            templateUrl: 'templates/tab-account.html',
            controller: 'AccountCtrl'
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/friends');

  });
