angular.module('starter.controllers', [])

  .controller('DashCtrl', function ($scope) {
  })

  .controller('ChatsCtrl', function ($scope, Chats) {
    $scope.chats = Chats.all();
    $scope.remove = function (chat) {
      Chats.remove(chat);
    }
  })

  .controller('ChatDetailCtrl', function ($scope, $stateParams, Chats) {
    $scope.chat = Chats.get($stateParams.chatId);
  })

  .controller('FriendsCtrl', function ($scope, Friends) {
    $scope.friends = Friends.all();
  })

  .controller('FriendDetailCtrl', function ($scope, $stateParams, Friends) {
    $scope.friend = Friends.get($stateParams.friendId);
  })

  .controller('AccountCtrl', function ($scope) {
    $scope.settings = {
      enableFriends: true
    };
  })

  .controller('MapCtrl', function ($scope, KickerData) {
    KickerData.getAll($scope.options)
      .then(function (locations) {
        $scope.map.markers = locations;
      });

    $scope.map = {
      markerIcon: 'img/kicker-marker.png',
      center: {
        latitude: 51.456680,
        longitude: 7.009991
      },
      options: {scrollwheel: false},
      zoom: 16,
      models: []
    }
  });
